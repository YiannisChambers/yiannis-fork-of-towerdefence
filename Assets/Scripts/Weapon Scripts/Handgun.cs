﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Handgun : BaseWeapon {

	GameObject ch;

	public AudioClip dryFire;

	// Use this for initialization
	void Start ()
	{	
		gunAnim = GetComponent<Animation> ();
		gunAnim.Play ("Draw");
		ch = GameObject.FindGameObjectWithTag ("Crosshair");
		ch.SetActive (true);
		gunSound = GetComponent<AudioSource> ();

		//TEMP code to get the player for base weapon 
		GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
		foreach(GameObject go in gos)
		{
			if(go.layer == 8 && go.CompareTag("Player"))//go.layer=="LocalPlayer" && go.CompareTag("Player"))
			{
				player = go; 
			}
		} 

	

	}

	// Update is called once per frame
	void Update ()
	{
		UpdateAmmo ("∞", "∞");

		if (canFire) {
			if (!gunAnim.isPlaying) {
				//Debug.Log ("ANIMATION IS NOT PLAYING");
				if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0))) {
					isFiring = true;
				} else if ((Input.GetButtonUp ("Fire1") || Input.GetMouseButtonUp (0))) {
					isFiring = false;
				}


				if (Input.GetButtonDown ("Fire2")) {
					if (isADS) {
						gunAnim.Play ("UnADS");
						isADS = false;
					} else {
						gunAnim.Play ("ADS");
						isADS = true;
					}
				}


				if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0)) && currentAmmo > 0) {
					AudioSource gunSound = GetComponent<AudioSource> ();
					if (isADS) {
						gunAnim.Play ("ADSFire");
					} else {
						gunAnim.Play ("Hipfire");
					}
					playerShoot = transform.root.GetComponent<PlayerShoot> ();
					playerShoot.currentWeapon = this;
					playerShoot.Shoot ();
					//((PlayerShoot)FindObjectOfType (typeof(PlayerShoot))).Shoot ();
					muzzleFlash.Play ();
					gunSound.Play ();
					hasFired = true;
					PlayerHelper.GetPlayer ().GetComponent<PlayerMovement> ().Rotate (0f, recoilAmount);
					Debug.Log ("1Ammo is now " + currentAmmo);
					Debug.Log ("2Ammo is now " + currentAmmo);



				}
			}
		}


	}
}
