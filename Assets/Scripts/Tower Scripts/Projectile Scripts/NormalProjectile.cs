﻿using UnityEngine;
using System.Collections;
using System;

public class NormalProjectile : BaseProjectile {

    Vector3 mdirection;
    bool mfired;
    GameObject mlauncher;
    GameObject mtarget;
    public int mdamage;
	
	// Update is called once per frame
	void Update () {
        if (mfired)
        {
            transform.position += mdirection * (speed * Time.deltaTime);
        }

        Destroy(gameObject, 5);
    }

    public override void FireProjectile(GameObject launcher, GameObject target, int damage, float attackSpeed)
    {
        if (launcher && target)
        {
            mdirection = (target.transform.position - launcher.transform.position).normalized;
            mfired = true;
            mlauncher = launcher;
            mtarget = target;
            mdamage = damage;
        }
    }
	//TODO: figure out if this section should be a 	[Command]...
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
			other.gameObject.GetComponent<Enemy>().TakeDamage(mdamage); //.RemoveHealth(mdamage);
            Destroy(gameObject, 5);
        }
        
    }
}
