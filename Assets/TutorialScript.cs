﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
public class TutorialScript : NetworkBehaviour {
	//TODO: FIX SHOOTING TEST. IMPLEMENT ENEMY AND WAVE TEST

	public GameObject tutorialUI; //This should be a prefab, that can be instantiated.
	public bool isShowingUI = false;
	private GameObject currentlyShowingTutorialUI;
	private TutorialStage currentStage;

	int indexOfStage = 0;

	List<TutorialStage> stages = new List<TutorialStage> ();

	//Tutorial flags
	private bool wPressed = false;
	private bool sPressed = false;
	private bool aPressed = false;
	private bool dPressed = false;
	private bool spacePressed = false;

	private bool towerPlaced = false;

	private bool storeOpened = false;
	private bool weaponBought = false;
	private bool weaponShot = false;

	private bool enemyKilled = false;
	private bool enemyWasAlive = false;

	private bool isEnterPressed = false;
	private bool isWaveOver = false;
	private bool hasWaveStarted = false;

	private Enemy enemy;

	// Use this for initialization
	void Start () {
		SetUpTutorialStages ();
		indexOfStage = 0;

		currentStage = stages.ToArray () [indexOfStage];
	}

	private void SetUpTutorialStages(){
		AddNewTutorialStage (TextDisplayComplete, "Welcome to the tutorial for The Crystal Guard!", "Welcome!");
		AddNewTutorialStage (TextDisplayComplete, "The year is 2124. Your only source of energy is under attack by drones. Defend the crystal.", "Welcome!");
		AddNewTutorialStage (Stage1Complete, "Move using the WASD keys. Jump with Space.", "Move and jump.");
		AddNewTutorialStage (TextDisplayComplete, "You can deploy towers to help defend. Switch between them with Q and E.", "Place a tower.");
		AddNewTutorialStage (Stage2Complete, "Place a tower by looking down at the ground, and clicking the Left Mouse Button.", "Place a tower.");
		AddNewTutorialStage (TextDisplayComplete, "If you ever want to get rid of a tower, you can sell it by pressing the Right Mouse Button.", "Selling of Towers");
		AddNewTutorialStage (TextDisplayComplete, "Towers alone won't be enough. To help defend, you need weapons.", "Weapons!");
		AddNewTutorialStage (Stage3Complete, "To buy a weapon, you need to open the Store. Open the Store by pressing the TAB button.", "Open the Store.");
		AddNewTutorialStage (Stage4Complete, "Buy a weapon, and some ammo, and then exit the store.", "Buy a weapon.");
		AddNewTutorialStage (Stage5Complete, "Take your weapon out for a test drive. Fire your weapon by pressing the Left Mouse Button.", "Fire your weapon.");
		AddNewTutorialStage (Stage6Complete, "Now it's time for your first enemy. Destroy the enemy drone!", "Destroy the drone.");
		AddNewTutorialStage (Stage7Complete, "Let's see how you handle your first wave of enemies. Press Enter when you're ready.", "Enemies incoming!");
		AddNewTutorialStage (TextDisplayComplete, "Congratulations! You have finished the tutorial. Return to the Main Menu.", "You did it!");

	}

	void AddNewTutorialStage(TutorialBoolean booleanMethod, string prompt, string phasePrompt){
		TutorialStage stage = new TutorialStage (booleanMethod, prompt, phasePrompt);
		stages.Add (stage);
	}
	
	// Update is called once per frame
	void Update () {
        if (!isServer) return;
		if (isShowingUI) {

			if (currentlyShowingTutorialUI.GetComponent<TutorialUIScript> ().isButtonClicked) {
				CloseUIWindow ();
				isShowingUI = false;

				if (indexOfStage == stages.Count - 1) {
					PlayerHelper.GetPlayer ().GetComponent<PlayerMovement> ().isFrozen = false;
					PlayerHelper.GetPlayer ().GetComponent<PlayerMovement> ().mouseLock = false;
                    Cursor.lockState = CursorLockMode.Confined;
                    Cursor.visible = true;
                    NetworkManager.Shutdown();
                    NetworkServer.DisconnectAll();
                    NetworkServer.Shutdown();
					SceneManager.LoadScene ("MainMenu");
				}
			}

			PlayerHelper.GetPlayer ().GetComponent<PlayerMovement> ().isFrozen = true;
			PlayerHelper.GetPlayer ().GetComponent<PlayerMovement> ().mouseLock = true;

		}else {

			PlayerHelper.GetPlayer ().GetComponent<PlayerMovement> ().isFrozen = false;
			PlayerHelper.GetPlayer ().GetComponent<PlayerMovement> ().mouseLock = false;

			if (!currentStage.hasShownPrompt) {
				SpawnUIWindow (currentStage.GetPrompts (), "Close");
				currentStage.hasShownPrompt = true;
			}
			else {
				UpdateFlags ();
				if (currentStage.IsCompleted ()) {
					currentStage.hasBeenCompleted = true;
					indexOfStage =  indexOfStage + 1;
					currentStage = stages.ToArray () [indexOfStage];

					if (indexOfStage == stages.Count - 3) {
						EnemyManager.singleton.SpawnEnemy(Enemy.FLYING, new Vector3(3, .5f, 0), 5);
						enemy = EnemyManager.GetClosestEnemy (PlayerHelper.GetPlayer ().transform.position);
						enemyWasAlive = true;
					}

					if (indexOfStage == stages.Count - 2) {
						;
					}

					//SpawnUIWindow ("Well done.", "Next");
				}
			}

		}

		//Stops tutorial from failing.
		GameObject.FindGameObjectWithTag ("Base").GetComponent<Base> ().currentHealth = 100;

	}

	public void CloseUIWindow(){
		currentlyShowingTutorialUI.SetActive (false);
		Destroy(currentlyShowingTutorialUI);	
	}

	private void SpawnUIWindow(string text){
		SpawnUIWindow (text, "Next");
	}

	private void SpawnUIWindow(string text, string buttonText){
		currentlyShowingTutorialUI  = (GameObject)(Instantiate (tutorialUI, new Vector3(), new Quaternion(), this.transform));
		currentlyShowingTutorialUI.GetComponent<TutorialUIScript> ().SetUIText (text, buttonText);
		isShowingUI = true;
		GameObject.FindGameObjectWithTag ("PlayerUI").GetComponent<PlayerUI> ().txtPhase.text = currentStage.GetPhasePrompt ();

	}



	private void UpdateFlags(){

		//Movement test
		if (Input.GetKeyDown (KeyCode.W)) {
			wPressed = true;
		}
		if (Input.GetKeyDown (KeyCode.S)) {
			sPressed = true;
		}
		if (Input.GetKeyDown (KeyCode.A)) {
			aPressed = true;
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			dPressed = true;
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			spacePressed = true;
		}

	
		//Tower placing test
		if (Stage1Complete ()) {
			if (TowerManager.GetClosestTower (PlayerHelper.GetPlayer ().transform.position) != null) {
				towerPlaced = true;
			}
		}

		//Entering shop test
		if (Stage2Complete ()) {
			if (GameObject.FindGameObjectWithTag ("PlayerUI").GetComponent<PlayerUI> ().showShop) {
				storeOpened = true;
			}
		}

		//Buying weapon test
		if (Stage3Complete ()) {
			WeaponManger weaponManager = PlayerHelper.GetPlayer ().GetComponent<WeaponManger> ();
			if (weaponManager.GetCurrentWeapon ().gunID != 0) {
				weaponBought = true;
			}
		}

		//Shooting weapon test
		if (Stage4Complete ()) {
			WeaponManger weaponManager = PlayerHelper.GetPlayer ().GetComponent<WeaponManger> ();
			BaseWeapon currentWeapon = weaponManager.GetCurrentWeapon ();
			if (currentWeapon.gunID != 0 && Input.GetMouseButton(0)) {
				weaponShot = true;
			}
		}

		//Killing enemy test
		if (Stage5Complete ()) {
			if (enemy != null) {
				if (enemy.isDead) {
					enemyKilled = true;
				}
			} else {
				if (enemyWasAlive) {
					enemyKilled = true;
				}
			}
		}

		if (Stage6Complete ()) {
			if (Input.GetKeyDown (KeyCode.Return)) {
				isEnterPressed = true;
			}

			if (isEnterPressed){
				if (GameManager.singleton.state == GameManager.GameState.PHASE_FIGHTING) {
					hasWaveStarted = true;
				} else {
					if (hasWaveStarted) {
						if (GameManager.singleton.state == GameManager.GameState.PHASE_BUILDING) {
							isWaveOver = true;
						}
					}
				}

			}
		}
	}


	public bool TextDisplayComplete(){
		return true;
	}

	public bool Stage1Complete(){
		return wPressed && sPressed && aPressed && dPressed && spacePressed;
	}

	public bool Stage2Complete(){
		return towerPlaced;
	}

	public bool Stage3Complete(){
		return storeOpened;
	}

	public bool Stage4Complete(){
		return weaponBought && GameObject.FindGameObjectWithTag("PlayerUI").GetComponent<PlayerUI>().showShop == false;
	}

	public bool Stage5Complete(){
		WeaponManger weaponManager = PlayerHelper.GetPlayer ().GetComponent<WeaponManger> ();
		BaseWeapon currentWeapon = weaponManager.GetCurrentWeapon ();
		return weaponShot && !currentWeapon.gunAnim.isPlaying;
	}

	public bool Stage6Complete(){
		return enemyKilled;
	}

	public bool Stage7Complete(){
		return isEnterPressed && isWaveOver;
	}


}
